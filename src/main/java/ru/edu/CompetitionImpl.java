package ru.edu;

import ru.edu.model.Athlete;
import ru.edu.model.CountryParticipant;
import ru.edu.model.Participant;

import java.util.*;

/**
 * Класс с информацией о соревнованиях.
 * Регистрация участников, обновление баллов участников,
 * получение рейтинга участников и стран-участников соревнования.
 */
public class CompetitionImpl implements Competition{

    /**
     * Переменная-счетчик для создания регистрационного номера участника.
     */
    private long id = 0;

    /**
     * Список зарегистрированных участников соревнования.
     */
    private final Map<Long, MyParticipant> participantsMap = new HashMap<>();

    /**
     * Список атлетов, участвующих в соревновании.
     * С ключом по регистрационному номеру.
     */
    private final Map<Long, Athlete> athletesMap = new HashMap<>();

    /**
     * Список атлетов, участвующих в соревновании.
     */
    private final Set<Athlete> registeredAthletes = new HashSet<>();

    /**
     * Список стран, участвующих в соревнованиях.
     */
    private final Map<String, MyCountryParticipant> countriesMap = new HashMap<>();

    /**
     * Регистрация участника.
     *
     * @param athlete - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */
    @Override
    public Participant register(Athlete athlete) {

        if(registeredAthletes.contains(athlete)){
            throw new IllegalStateException("Such athlete is already registered.");
        }

        registeredAthletes.add(athlete);

        MyParticipant myParticipant = new MyParticipant(++id, athlete);

        participantsMap.put(myParticipant.id, myParticipant);
        athletesMap.put(myParticipant.id, athlete);

        if (countriesMap.containsKey(athlete.getCountry())) {
            countriesMap.get(athlete.getCountry()).addCountryParticipants(myParticipant);
        } else {
            MyCountryParticipant myCountryParticipant =
                    new MyCountryParticipant(athlete.getCountry());
            myCountryParticipant.addCountryParticipants(myParticipant);
            countriesMap.put(athlete.getCountry(), myCountryParticipant);
        }

        return myParticipant.getClone();
    }

    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения.
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param id    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    @Override
    public void updateScore(long id, long score) {
        MyParticipant myParticipant = participantsMap.get(id);

        if (myParticipant == null) {
            throw new IllegalArgumentException("A participant with this id is not registered.");
        }

        myParticipant.incrementScore(score);
        countriesMap.get(myParticipant.getAthlete().getCountry()).updateScore(score);

    }

    /**
     * Обновление счета участника по его объекту Participant.
     * Требуется константное время выполнения.
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */
    @Override
    public void updateScore(Participant participant, long score) {
        updateScore(participant.getId(), score);
    }

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */
    @Override
    public List<Participant> getResults() {
        LinkedList<Participant> myParticipants = new LinkedList<>(participantsMap.values());
        myParticipants.sort(((o1, o2) -> {

            if (o1.getScore() == o2.getScore()) {
                return 0;
            }

            return (o1.getScore() < o2.getScore()) ? 1 : -1;
        }));

        return myParticipants;

    }

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */
    @Override
    public List<CountryParticipant> getParticipantsCountries() {
        LinkedList<CountryParticipant> myCountryParticipants =
                new LinkedList<>(countriesMap.values());
        myCountryParticipants.sort(((o1, o2) -> {

            if (o1.getScore() == o2.getScore()) {
                return 0;
            }

            return (o1.getScore() < o2.getScore()) ? 1 : -1;
        }));

        return myCountryParticipants;

    }

    /**
     * Получение списка атлетов, участвующих в соревнованиях.
     *
     * @return список участников
     */
    public Map<Long, Athlete> getAthletesList() {
        return athletesMap;
    }

    /**
     * Метод для получения полного имени участника из его имени и / или фамилии.
     * @param fName - имя участника
     * @param lName - фамилия участника
     * @return полное имя участника
     */
    public static String makeFullName(String fName, String lName) {

        return (fName.length() == 0) ? lName :
                    ((lName.length() == 0) ? fName :
                        String.join(" ", fName, lName));

    }

    /**
     * Класс для сохранения информации об участнике соревнований.
     */
    private static class MyParticipant implements Participant {

        /**
         * Регистрационный номер участника.
         */
        public final Long id;

        /**
         * Счет участника.
         */
        private long score;

        /**
         * Информация об участнике.
         */
        private final Athlete athlete;

        public MyParticipant(long id, Athlete athlete) {
            this.id = id;
            this.athlete = athlete;
        }

        /**
         * Получение информации о регистрационном номере участника.
         *
         * @return регистрационный номер
         */
        @Override
        public Long getId() {
            return id;
        }

        /**
         * Метод для получения информации о спортсмене.
         *
         * @return объект спортсмена
         */
        @Override
        public Athlete getAthlete() {
            return athlete;
        }

        /**
         *
         * Метод для обновления счета участника.
         *
         * @param value - количество баллов, на которое надо увеличить или уменьшить счет.
         */
        public void incrementScore(long value){
            score += value;
        }

        /**
         * Счет участника.
         *
         * @return счет
         */
        @Override
        public long getScore() {
            return score;
        }

        /**
         * Метод для клонирования объекта участника соревнований.
         *
         * @return MyParticipant - копию объекта участника соревнований.
         */
        public MyParticipant getClone(long partId, Athlete partAthlete) {
            return new MyParticipant(partId, partAthlete);
        }

        public MyParticipant getClone() {
            return getClone(this.id, this.athlete);
        }

        @Override
        public String toString() {

            String temp;
            temp = "Участник (ID = " + id + "): ";
            temp += athlete.toString();
            temp += ", счет = " + score;
            return temp;

        }
    }

    /**
     * Класс для сохранения информации об стране-участнике соревнований.
     */
    private static class MyCountryParticipant implements CountryParticipant {

        /**
         * Название страны.
         */
        private final String name;

        /**
         * Счет страны.
         */
        private long score;

        /**
         * Список участников от страны.
         */
        private final List<Participant> countryParticipants;

        MyCountryParticipant(String name) {
            this.score = 0;
            this.name = name;
            this.countryParticipants = new ArrayList<>();
        }

        /**
         * Название страны.
         *
         * @return название страны.
         */
        @Override
        public String getName() {
            return name;
        }

        /**
         * Метод для добавления участника в список участников его страны.
         *
         * @param participant - объект участника соревнований.
         */
        public void addCountryParticipants(Participant participant) {
            this.countryParticipants.add(participant);
        }

        /**
         * Список участников от страны.
         *
         * @return список участников соревнований от страны.
         */
        @Override
        public List<Participant> getParticipants() {
            return countryParticipants;
        }

        /**
         * Метод для обновления общего счета страны.
         *
         * @param score - количество баллов, на которое надо увеличить или уменьшить счет.
         */
        public void updateScore(long score) {
            this.score += score;
        }

        /**
         * Счет страны.
         *
         * @return счет
         */
        @Override
        public long getScore() {
            return score;
        }

        /**
         * Вывод на печать информации о стране-участнике соревнований.
         *
         * @return - информация о стране.
         */
        @Override
        public String toString() {

            StringBuilder temp = new StringBuilder();
            StringBuilder athletesNames = new StringBuilder();

            temp.append("Страна ");
            temp.append(name);
            temp.append(" (счет = ");
            temp.append(score);
            temp.append("), участники: ");

            for (Participant p : countryParticipants) {

                if (athletesNames.length() > 0) {
                    athletesNames.append(", ");
                }
                athletesNames.append(makeFullName(
                        p.getAthlete().getFirstName(),
                        p.getAthlete().getLastName()));

            }

            return temp.append(athletesNames).toString();

        }
    }


}
