package ru.edu.model;

import java.util.Objects;

/**
 * Класс с информацией об атлете.
 */
public class AthleteImpl implements Athlete{

    private final String firstName;
    private final String lastName;
    private final String country;

    private AthleteImpl(String firstName, String lastName, String country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Имя.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return country;
    }

    public static class Builder {

        private String firstName = "";
        private String lastName = "";
        private String country = "";

        public Builder setFirstName(String input) {
            this.firstName = input;
            return this;
        }

        public Builder setLastName(String input) {
            this.lastName = input;
            return this;
        }

        public Builder setCountry(String input) {
            this.country = input;
            return this;
        }

        public AthleteImpl build() {

            if ((firstName.length() == 0) && (lastName.length() == 0)) {
                throw new IllegalArgumentException("" +
                        "At least one part of the athlete's name must be entered to register.");
            }

            if (country.length() == 0) {
                throw new IllegalArgumentException(
                        "The country is required for the registration of the athlete.");
            }

            return new AthleteImpl(firstName, lastName, country);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AthleteImpl)) return false;
        AthleteImpl athlete = (AthleteImpl) o;
        return Objects.equals(firstName, athlete.firstName)
                && Objects.equals(lastName, athlete.lastName)
                && Objects.equals(country, athlete.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, country);
    }

    @Override
    public String toString() {

        String temp = "";
        temp += (firstName.length() == 0) ? lastName :
                ((lastName.length() == 0) ? firstName : String.join(" ", firstName, lastName));
        temp += " (" + country + ")";
        return temp;
    }

}
