package ru.edu.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class AthleteImplTest {

    Athlete athlete, athlete2, athlete3, athlete4, athlete5;

    @Test(expected = IllegalArgumentException.class)
    public void testBuilderWithoutName() {

        AthleteImpl.builder()
                 .setCountry("Россия")
                .build();

    }


    @Test(expected = IllegalArgumentException.class)
    public void testBuilderWithoutCountry() {

        AthleteImpl.builder()
                .setFirstName("Ольга")
                .build();

    }

    @Test
    public void testBuilder() {

        athlete = AthleteImpl.builder()
                .setFirstName("Ольга")
                .setLastName("Хромова")
                .setCountry("Россия")
                .build();

        athlete2 = AthleteImpl.builder()
                .setFirstName("Кассандра")
                .setCountry("Украина")
                .build();

        athlete3 = AthleteImpl.builder()
                .setLastName("Иванов")
                .setCountry("Белоруссия")
                .build();

        athlete4 = AthleteImpl.builder()
                .setFirstName("Анна")
                .setLastName("Хромова")
                .setCountry("Россия")
                .build();

        athlete5 = AthleteImpl.builder()
                .setLastName("Иванов")
                .setCountry("Украина")
                .build();

    }


    @Test
    public void testToString() {

        testBuilder();
        System.out.println(athlete);
        System.out.println(athlete2);
        System.out.println(athlete3);
        System.out.println(athlete4);
        System.out.println(athlete5);

    }

    @Test
    public void testGetFirstName() {

        testBuilder();
        assertEquals("Ольга", athlete.getFirstName());
        assertEquals("Кассандра", athlete2.getFirstName());

    }

    @Test
    public void testGetLastName() {

        testBuilder();
        assertEquals("Хромова", athlete.getLastName());
        assertEquals("", athlete2.getLastName());

    }

    @Test
    public void testGetCountry() {

        testBuilder();
        assertEquals("Россия", athlete.getCountry());
        assertEquals("Украина", athlete2.getCountry());

    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    public void testEquals() {

        testBuilder();

        //noinspection EqualsWithItself
        assertTrue(athlete.equals(athlete));
        assertFalse(athlete.equals(athlete2));
        assertFalse(athlete2.equals(athlete3));
        assertFalse(athlete.equals(athlete4));

        assertTrue(athlete.equals(AthleteImpl
                .builder()
                .setFirstName("Ольга")
                .setLastName("Хромова")
                .setCountry("Россия")
                .build()));

    }
}