package ru.edu;

import org.junit.Test;
import ru.edu.model.Athlete;
import ru.edu.model.AthleteImpl;
import ru.edu.model.CountryParticipant;
import ru.edu.model.Participant;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class CompetitionImplTest {

    final static String[][] ATHLETES = {
            {"Дина", "Аверина", "Россия"}, // здесь информация об атлете-победителе
            {"Даниил", "Медведев", "Россия"},
            {"Андрей", "Рублев", "Россия"},
            {"Кассандра", "", "Украина"},
            {"", "Иванов", "Болгария"},
            {"Джон", "Смит", "США"},
    };

    final static long[][] SCORES = {
            {100, 150, 250}, // здесь максимальная сумма очков для атлета-победителя
            {200},
            {300},
            {33},
            {200},
            {444}
    };

    CompetitionImpl competition = new CompetitionImpl();
    LinkedList<Participant> myParticipants = new LinkedList<>();

    @Test(expected = IllegalStateException.class)
    public void testDoubleRegistration() {

        myParticipants.add(competition.register(
                AthleteImpl.
                        builder().
                        setFirstName(ATHLETES[0][0]).
                        setLastName(ATHLETES[0][1]).
                        setCountry(ATHLETES[0][2])
                        .build()));

        myParticipants.add(competition.register(
                AthleteImpl.
                        builder().
                        setFirstName(ATHLETES[0][0]).
                        setLastName(ATHLETES[0][1]).
                        setCountry(ATHLETES[0][2])
                        .build()));

    }

    @Test
    public void testRegister() {

        for (String[] athlete : ATHLETES) {
            myParticipants.add(competition.register(
                    AthleteImpl.
                            builder().
                            setFirstName(athlete[0]).
                            setLastName(athlete[1]).
                            setCountry(athlete[2])
                            .build()));
        }

        assertEquals(ATHLETES.length, myParticipants.size());

    }

    @Test
    public void testToString() {

        testRegister();
        for (Participant p : myParticipants) {
            System.out.println(p);
        }

    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateUnknownParticipant() {
        competition.updateScore(1, 5);
    }

    @Test
    public void testUpdateScore() {

        testRegister();
        for (int i = 0; i < SCORES.length; i++) {
            for (int j = 0; j < SCORES[i].length; j++) {
                competition.updateScore((i + 1), SCORES[i][j]);
            }
        }

        assertEquals(Arrays.stream(SCORES[0]).sum(), competition.getResults().get(0).getScore());

    }


    @Test
    public void testGetResults() {

        testUpdateScore();

        List<Participant> results;
        results = competition.getResults();

        assertEquals(6, results.size());

        assertEquals(Arrays.stream(SCORES[0]).sum(), results.get(0).getScore());
        assertEquals(ATHLETES[0][0], results.get(0).getAthlete().getFirstName());
        assertEquals(ATHLETES[0][1], results.get(0).getAthlete().getLastName());
        assertEquals(ATHLETES[0][2], results.get(0).getAthlete().getCountry());

        for (Participant p : results) {
            System.out.println(p);
        }

    }

    @Test
    public void testGetParticipantsCountries() {

        testUpdateScore();

        List<CountryParticipant> results;
        results = competition.getParticipantsCountries();

        assertEquals(4, results.size());

        assertEquals("Россия", results.get(0).getName());
        assertEquals(1000, results.get(0).getScore());
        assertEquals(3, results.get(0).getParticipants().size());

        for (CountryParticipant c : results) {
            System.out.println(c);
        }

    }

    @Test
    public void testGetAthletesList() {

        testRegister();

        Map<Long, Athlete> athletesMap;
        athletesMap = competition.getAthletesList();
        assertEquals(ATHLETES.length, athletesMap.size());
        System.out.println(athletesMap);
    }
}